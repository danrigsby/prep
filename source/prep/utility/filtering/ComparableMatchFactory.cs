﻿using System;

namespace prep.utility.filtering
{
  public class ComparableMatchFactory<ItemToMatch, PropertyType> : ICreateMatchers<ItemToMatch, PropertyType>
    where PropertyType : IComparable<PropertyType>
  {
    ICreateMatchers<ItemToMatch, PropertyType> original;

    public ComparableMatchFactory(ICreateMatchers<ItemToMatch, PropertyType> original)
    {
      this.original = original;
    }

    public IMatchAn<ItemToMatch> equal_to(PropertyType value)
    {
      return original.equal_to(value);
    }

    public IMatchAn<ItemToMatch> equal_to_any(params PropertyType[] values)
    {
      return original.equal_to_any(values);
    }

    public IMatchAn<ItemToMatch> not_equal_to(PropertyType value)
    {
      return original.not_equal_to(value);
    }

    public IMatchAn<ItemToMatch> greater_than(PropertyType value)
    {
      return create_using(new IsGreaterThan<PropertyType>(value));
    }

    public IMatchAn<ItemToMatch> create_using(IMatchAn<PropertyType> real_matcher)
    {
      return original.create_using(real_matcher);
    }

    public IMatchAn<ItemToMatch> between(PropertyType start, PropertyType end)
    {
      return create_using(new IsBetween<PropertyType>(start, end));
    }
  }
}