namespace prep.utility.filtering
{
  public interface ICreateMatchers<in ItemToMatch, PropertyType>
  {
    IMatchAn<ItemToMatch> equal_to(PropertyType value);
    IMatchAn<ItemToMatch> equal_to_any(params PropertyType[] values);
    IMatchAn<ItemToMatch> not_equal_to(PropertyType value);
    IMatchAn<ItemToMatch> create_using(IMatchAn<PropertyType> real_matcher);
  }
}