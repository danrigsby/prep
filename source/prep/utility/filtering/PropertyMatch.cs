﻿namespace prep.utility.filtering
{
  public class PropertyMatch<ItemToMatch,PropertyType> : IMatchAn<ItemToMatch>
  {
    PropertyAccessor<ItemToMatch, PropertyType> accessor;
    IMatchAn<PropertyType> real_criteria;

    public PropertyMatch(PropertyAccessor<ItemToMatch, PropertyType> accessor, IMatchAn<PropertyType> real_criteria)
    {
      this.accessor = accessor;
      this.real_criteria = real_criteria;
    }

    public bool matches(ItemToMatch item)
    {
      return real_criteria.matches(accessor(item));
    }
  }
}